#!/usr/bin/env python3
import json
import os.path
import subprocess
import sys
from glob import glob

from jinja2 import Environment, FileSystemLoader


def render_from_template(directory, template_name, **kwargs):
    loader = FileSystemLoader(directory)
    env = Environment(loader=loader, autoescape=False)
    template = env.get_template(template_name)
    return template.render(**kwargs)


# https://bitbucket.org/leonidas/jinja2-autoescape/src/434d9b0b3b4d/jinja2/utils.py?fileviewer=file-view-default#cl-497
def escapetex(string):
    print(string)
    string = string.replace("\\", "\\textbackslash{}")
    string = string.replace("&", "\\&")
    string = string.replace("\n", "\\\\")
    string = string.replace("%", "\\%")
    string = string.replace("$", "\\$")
    string = string.replace("#", "\\#")
    string = string.replace("_", "\\_")
    string = string.replace("{", "\\{")
    string = string.replace("}", "\\}")
    string = string.replace("~", "\\")
    string = string.replace("^", "\\^")
    print(string)
    return string


def recursiveescape(obj):
    if type(obj) == str:
        return escapetex(obj)
    elif type(obj) == list:
        return [recursiveescape(o) for o in obj]
    elif type(obj) == dict:
        return {i: recursiveescape(obj[i]) for i in obj}
    elif type(obj) in [int, float, bool] or obj is None:
        return obj
    else:
        raise NotImplementedError("Unsupported type: " + str(type(obj)))


os.chdir(os.path.dirname(sys.argv[0]))
if not os.path.exists('dst'):
    os.mkdirs('dst')
for filename in glob('*.json'):
    langname = filename[:-5]
    with open(filename) as f:
        data = json.load(f)
    data['langname'] = langname
    # data = recursiveescape(data)
    outfilename = langname+'.tex'
    with open(os.path.join('dst', outfilename), 'w') as f:
        f.write(render_from_template('.', 'template.tex.tpl', **data))
    subprocess.call(
        ['pdflatex', '-interaction=nonstopmode', outfilename], cwd='dst')

# clean up latex-files
tmp = [x for x in glob('dst/*') if x not in glob('dst/*pdf')]
subprocess.call(['rm'] + tmp)
