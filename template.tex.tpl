%        File: template.tex
%     Created: Mo Mai 28 12:00  2018 C
% Last Change: Mo Mai 28 12:00  2018 C
%
\documentclass[10pt,a4paper]{article}
\usepackage{makecell}
\usepackage{xcolor}
    \definecolor{background}{HTML}{F2F0E8}
\usepackage{array}
\usepackage{colortbl}
\usepackage[a4paper,includeheadfoot,margin=1.54cm]{geometry}
\usepackage{listings}
	\lstset{
	  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
	  language={{langname}},
	  keywordstyle=\color{blue},
	  breaklines=true,
	  postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hookrightarrow\space}},
	  morekeywords={},	
	  commentstyle=\color{brown},
	  alsoletter={},
	}
\definecolor{Background1}{rgb}{0.95,0.90,0.95}
\definecolor{Background2}{rgb}{0.90,0.95,0.95}
\definecolor{LightCyan}{rgb}{0.88,1,1}
\arrayrulecolor{gray}
\newcolumntype{a}{>{\columncolor{Background1}}m{5cm}}
\newcolumntype{b}{>{\columncolor{Background2}}m{9cm}}
\begin{document}
\pagestyle{empty}
\section*{ {{langname|title}} }
	

\begin{table}[h!]
	\centering
	\begin{tabular}{a|b}\hline
		Import      &
		{\begin{lstlisting}
{{import}}
			\end{lstlisting}
		}\\ \hline

		Print-Command   &
		{\begin{lstlisting}
{{print}}
			\end{lstlisting}
		}\\ \hline

		Index starts at &
		{\begin{lstlisting}
{{indexstart}}
			\end{lstlisting}
		}\\ \hline

		End of command  &
		{\begin{lstlisting}
{{commandend}}
			\end{lstlisting}
		}\\ \hline

		Logic operators &
		{\begin{lstlisting}
{% for op in logicops %}{{op}}  {% endfor %}
			\end{lstlisting}
		}\\ \hline

		Exponent        &
		{\begin{lstlisting}
{{exponent}}
			\end{lstlisting}
		}\\ \hline

		Code block      &
		{\begin{lstlisting}
{{codeblock}}
			\end{lstlisting}
		}\\ \hline

		Whitespace sensitive      &
{{whitspace}}
		}\\ \hline

		Variable definition      &
		{\begin{lstlisting}
{{vardef}}
			\end{lstlisting}
		}\\ \hline

		Function definition      &
		{\begin{lstlisting}
{{funcdef}}
			\end{lstlisting}
		}\\ \hline

		Available types of loops &
		{\begin{lstlisting}
{% for type in loops %}
{{type}}
{% endfor %}
			\end{lstlisting}
		}\\ \hline

		Body-prototype      &
		{\begin{lstlisting}
{{body}}
			\end{lstlisting}
		}\\ \hline

		Convert String to Int      &
		{\begin{lstlisting}
{{str2int}}
			\end{lstlisting}
		}\\ \hline

		String concatenation      &
		{\begin{lstlisting}
{{strconcat}}
			\end{lstlisting}
		}\\ \hline

	\end{tabular}
\end{table}

\end{document}


